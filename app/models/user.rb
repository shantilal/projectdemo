class User < ActiveRecord::Base


	#validation of model
	 extend FriendlyId
  friendly_id :slug_candidates, use: [:slugged, :finders]

  
	validates :username, presence: true

	Regular_Expr = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: { with: Regular_Expr }

	has_secure_password


  has_many :active_relationships, class_name:  "Relationship",
                                  foreign_key: "follower_id",
                                  dependent:   :destroy

 has_many :passive_relationships, class_name:  "Relationship",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
 has_many :following, through: :active_relationships,  source: :followed
 has_many :followers, through: :passive_relationships, source: :follower

 has_many :albums,dependent: :destroy

#images
  
 has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/




def slug_candidates
    [
      :username,
      [:username, :id]
    ]
  end
  
  def to_param
   username.parameterize
  end



# Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end




#facebook login
 	def self.from_omniauth(auth)
  	
		user=User.where(provider: auth.provider, uid: auth.uid).first_or_initialize do |user|
    	user.username=auth.info.name
    	user.email=auth.info.email
    	user.location=auth.info.location
      user.avatar=process_uri(auth.info.image)

    	user.password_digest=User.digest("Password")
      end
      user.save!

      return user
	end

def self.process_uri(uri)
   avatar_url = URI.parse(uri)
   avatar_url.scheme = 'https'
   avatar_url.to_s
end


  # Follows a user.
  def follow(other_user)
    active_relationships.create(followed_id: other_user.id)
  end

  # Unfollows a user.
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    following.include?(other_user)
  end

	 
end
