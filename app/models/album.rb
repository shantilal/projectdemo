class Album < ActiveRecord::Base
  belongs_to :user

  has_many :images ,dependent: :destroy

  accepts_nested_attributes_for :images,:allow_destroy => true 

  validates :user_id, presence: true
  validates :album_name, presence: true
  validates :visibility, presence: true


  
end
