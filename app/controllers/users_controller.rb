class UsersController < ApplicationController
  
  def new
  	@user=User.new
  
  end

  def show
     @user = User.friendly.find(params[:id])
    


  end

  def create
    @user = User.new(user_params)    # Not the final implementation!
    if @user.save
      log_in @user
      current_user
      flash[:success] = "Welcome to the My App!"
      redirect_to @user
      
    else
      render 'new'
    end
  end

  def edit 
    @user=User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
      if @user.update_attributes(user_params)
          flash[:success] = "Profile updated"
        redirect_to @user
      else
        render 'edit'
      end
  end

  def index
    @user=User.all
  end

  def facebook
     @user=User.from_omniauth(request.env["omniauth.auth"])
          
    if @user.persisted?
      redirect_to @user
    else
       redirect_to register_path
    end
  end



  private

    def user_params
      params.require(:user).permit(:username, :email,:location, :password,
                                   :password_confirmation,:avatar)
    end

end
