class CreateAlbums < ActiveRecord::Migration
  def change
    create_table :albums do |t|
      t.string :album_name
      t.string :visibility
      t.references :user, index: true

      t.timestamps
    end
      add_index :albums, [:user_id, :created_at]
  end
end
